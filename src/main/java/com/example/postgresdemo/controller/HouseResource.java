package com.example.postgresdemo.controller;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import com.example.postgresdemo.model.House;
import com.example.postgresdemo.model.Log;
import com.example.postgresdemo.repository.HouseRepository;
import com.example.postgresdemo.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
public class HouseResource {
    @Autowired
    private HouseRepository houseRepository;
    @Autowired
    private LogRepository logRepository;
    @Autowired
    private Log log;

    @GetMapping("/houses")
    public List<House> retrieveAllHouses() {
        log.setStatus(" find all houses");
        log.setTime(new SimpleDateFormat("yyyy.MM.dd  HH:mm:ss").format(Calendar.getInstance().getTime()));
        logRepository.save(log);
        return houseRepository.findAll();
    }

    @GetMapping("/houses/{id}")
    public House retrieveHouse(@PathVariable long id) {
        Optional<House> house = houseRepository.findById(id);
        log.setStatus(" find house by" + id + "id");
        log.setTime(new SimpleDateFormat("yyyy.MM.dd  HH:mm:ss").format(Calendar.getInstance().getTime()));
        logRepository.save(log);
        if (!house.isPresent()) {
            System.out.println("id not find");
            return null;
        }

        return house.get();
    }

    @GetMapping("/logs/{id}")
    public Log retrieveLog(@PathVariable long id) {
        Optional<Log> logOptional = logRepository.findById(id);
        if (!logOptional.isPresent()) {
            System.out.println("id not find");
            return null;
        }

        return logOptional.get();
    }

    @DeleteMapping("/houses/{id}")
    public void deleteHouse(@PathVariable long id) {
        houseRepository.deleteById(id);
        log.setStatus("delete house by " + id + " id");
        log.setTime(new SimpleDateFormat("yyyy.MM.dd  HH:mm:ss").format(Calendar.getInstance().getTime()));
        logRepository.save(log);
    }

    @PostMapping("/houses")
    public ResponseEntity<Object> createHouse(@RequestBody House house) {
        house.setNumber(house.getApartemantList().size());
        House savedHouse = houseRepository.save(house);
        log.setStatus("create house by " + house.getId() + " id");
        log.setTime(new SimpleDateFormat("yyyy.MM.dd  HH:mm:ss").format(Calendar.getInstance().getTime()));
        logRepository.save(log);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(savedHouse.getId()).toUri();

        return ResponseEntity.created(location).build();

    }

    @PutMapping("/houses/{id}")
    public ResponseEntity<Object> updateHouse(@RequestBody House house, @PathVariable String name) {

        Optional<House> houseOptional = houseRepository.findById(house.getId());

        if (!houseOptional.isPresent()) return ResponseEntity.notFound().build();

        house.setHouseName(name);

        houseRepository.save(house);
        log.setStatus("update name house by " + house.getId() + " id");
        log.setTime(new SimpleDateFormat("yyyy.MM.dd  HH:mm:ss").format(Calendar.getInstance().getTime()));
        logRepository.save(log);
        return ResponseEntity.noContent().build();
    }

}
