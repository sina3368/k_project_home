package com.example.postgresdemo.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class House {
	private long id;
	private String houseName;
	private long remain;
	private Adress adress;
	private HouseAccount houseAccount;
	private List<HouseManeger> mangerList;
	private int floorNumber;
	private List<Apartemant> apartemantList;
	
	public House() {
		super();
	}
	
	public House( String houseName, long remain, Adress adress, HouseAccount houseAccount,
			List<HouseManeger> mangerList,List<Apartemant> apartemantList) {
		super();
		this.houseName = houseName;
		this.remain = remain;
		this.adress = adress;
		this.houseAccount = houseAccount;
		this.mangerList = mangerList;
		this.apartemantList=apartemantList;
		this.floorNumber=apartemantList.size();
	}


	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	} 
	
	@Column
	public String getHouseName() {
		return houseName;
	}
	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}
	
	@Column
	public long getRemain() {
		return remain;
	}

	public void setRemain(long remain) {
		this.remain = remain;
	}
	@OneToOne(cascade = CascadeType.ALL, targetEntity = Adress.class)
	@JoinColumn(name="adress_id")
	public Adress getAdress() {
		return adress;
	}

	public void setAdress(Adress adress) {
		this.adress = adress;
	}
	
	@OneToOne(cascade = CascadeType.ALL, targetEntity = HouseAccount.class)
	@JoinColumn(name="Account_id")
	public HouseAccount getHouseAccount() {
		return houseAccount;
	}

	public void setHouseAccount(HouseAccount houseAccount) {
		this.houseAccount = houseAccount;
	}

	@OneToMany(cascade = CascadeType.ALL, targetEntity = HouseManeger.class)
	@JoinColumn(name="maneger_id")
	public List<HouseManeger> getMangerList() {
		return mangerList;
	}


	public void setMangerList(List<HouseManeger> mangerList) {
		this.mangerList = mangerList;
	}

	@Column
	public int getNumber() {
		return floorNumber;
	}

	public void setNumber(int floorNumber) {
		this.floorNumber =floorNumber;
	}

	@OneToMany(cascade = CascadeType.ALL, targetEntity = Apartemant.class)
	@JoinColumn(name="house_id")
	public List<Apartemant> getApartemantList() {
		return apartemantList;
	}

	public void setApartemantList(List<Apartemant> apartemantList) {
		this.apartemantList = apartemantList;
	}

	}
