package com.example.postgresdemo.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.persistence.*;

@Entity
@Component
public class Log {

    private long id;
    private String status;
    private String time;

    public Log() {
    }

    public Log(String status, String time) {
        this.status = status;
        this.time = time;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column
    @Autowired
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(length = 10000)
    @Autowired
    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}

