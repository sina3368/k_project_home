package com.example.postgresdemo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class HouseAccount {
	
	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	private String nameBank;
	private long accountNumber;
	private long cardNumber;
	public HouseAccount() {
		super();
			}
	public HouseAccount( String nameBank, long accountNumber, long cardNumber) {
		super();
		this.nameBank = nameBank;
		this.accountNumber = accountNumber;
		this.cardNumber = cardNumber;
	}
	@Column
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	@Column
	public String getNameBank() {
		return nameBank;
	}
	public void setNameBank(String nameBank) {
		this.nameBank = nameBank;
	}
	@Column
	public long getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}
	@Column
	public long getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(long cardNumber) {
		this.cardNumber = cardNumber;
	}

}
