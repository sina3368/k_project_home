package com.example.postgresdemo.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Apartemant implements Serializable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	private Owner owner;
	private Roomer roomer;
	private String location;
	private long rimanIn;
	private int Space;
	private int personNumber;
	private long charge;
	public Apartemant() {
		super();
			}
	public Apartemant( Roomer roomer,Owner owner, String location, long rimanIn, int space, int personNumber,
			long charge) {
		super();
		this.owner = owner;
		this.roomer = roomer;
		this.location = location;
		this.rimanIn = rimanIn;
		Space = space;
		this.personNumber = personNumber;
		this.charge = charge;
	}
	
	@Column
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	@Column(length =10000)
	public Owner getOwner() {
		return owner;
	}
	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	@Column(length =10000)
	public Roomer getRoomer() {
		return roomer;
	}
	public void setRoomer(Roomer roomer) {
		this.roomer = roomer;
	}
	
	@Column
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	@Column
	public long getRimanIn() {
		return rimanIn;
	}
	public void setRimanIn(long rimanIn) {
		this.rimanIn = rimanIn;
	}
	
	@Column
	public int getSpace() {
		return Space;
	}
	public void setSpace(int space) {
		Space = space;
	}
	
	@Column
	public int getPersonNumber() {
		return personNumber;
	}
	public void setPersonNumber(int personNumber) {
		this.personNumber = personNumber;
	}
	
	@Column
	public long getCharge() {
		return charge;
	}
	public void setCharge(long charge) {
		this.charge = charge;
	}
	
}
