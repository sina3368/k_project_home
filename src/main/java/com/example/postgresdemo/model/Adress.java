package com.example.postgresdemo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class Adress {
	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	private String State;
	private String city;
	private String area;
	private String postAdress;
	private String postCode;
	
	public Adress() {
		super();
			}

	public Adress(String state, String city, String area, String postAdress, String postCode) {
		super();
		State = state;
		this.city = city;
		this.area = area;
		this.postAdress = postAdress;
		this.postCode = postCode;
	}
	@Column
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	@Column
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	@Column
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@Column
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	@Column
	public String getPostAdress() {
		return postAdress;
	}
	public void setPostAdress(String postAdress) {
		this.postAdress = postAdress;
	}
	@Column
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
}
