package com.example.postgresdemo.controller;

import com.example.postgresdemo.model.*;
import com.example.postgresdemo.repository.HouseRepository;
import com.example.postgresdemo.repository.LogRepository;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class HouseResourceTest {
    @Autowired
    private HouseRepository repository;
    @Autowired
    private LogRepository logRepository;

    @Test
    public void retrieveAllHouses() {
        Adress adress = new Adress("thran", "thran", "serag", "]eraghi", "123456");
        HouseManeger maneger1 = new HouseManeger("ali", "alavi", "091234567");
        HouseManeger maneger2 = new HouseManeger("hasan", "hasani", "091234444");
        List<HouseManeger> manegerList = new ArrayList<HouseManeger>();
        manegerList.add(maneger1);
        manegerList.add(maneger2);
        Owner owner = new Owner("ali", "alavi", "091234567");
        Roomer roomer = new Roomer("ali", "alavi", "091234567");
        Apartemant apartemant1 = new Apartemant(roomer, owner, "w", 2000, 100, 2, 200000);

        Owner owner1 = new Owner("ali", "alavi", "091234567");
        Roomer roomer1 = new Roomer("abas", "asadi", "09123400");
        Apartemant apartemant2 = new Apartemant(roomer1, owner1, "s", 3000, 200, 3, 200000);
        List<Apartemant> apartemantList = new ArrayList<Apartemant>();
        apartemantList.add(apartemant1);
        apartemantList.add(apartemant2);
        HouseAccount account = new HouseAccount("meli", 543, 876);
        House house = new House("abi", 20000, adress, account, manegerList, apartemantList);
        String time = new SimpleDateFormat("yyyy.MM.dd  HH:mm:ss").format(Calendar.getInstance().getTime());
        Log log = new Log(" find all houses", time);
        House result = repository.save(house);
        Log log1 = logRepository.save(log);
        assertEquals(" find all houses", log1.getStatus());
        assertEquals(time, log1.getTime());

        assertEquals("abi", result.getHouseName());
        assertEquals(20000, result.getRemain());
        assertEquals(2, result.getNumber());

        assertEquals("thran", result.getAdress().getState());
        assertEquals("thran", result.getAdress().getCity());
        assertEquals("serag", result.getAdress().getArea());
        assertEquals("]eraghi", result.getAdress().getPostAdress());
        assertEquals("123456", result.getAdress().getPostCode());

        assertEquals("meli", result.getHouseAccount().getNameBank());
        assertEquals(543, result.getHouseAccount().getAccountNumber());
        assertEquals(876, result.getHouseAccount().getCardNumber());

        assertEquals("ali", result.getMangerList().get(0).getFirstName());
        assertEquals("alavi", result.getMangerList().get(0).getLastName());
        assertEquals("091234567", result.getMangerList().get(0).getMobileNumber());


        assertEquals("hasan", result.getMangerList().get(1).getFirstName());
        assertEquals("hasani", result.getMangerList().get(1).getLastName());
        assertEquals("091234444", result.getMangerList().get(1).getMobileNumber());


        assertEquals("ali", result.getApartemantList().get(0).getOwner().getFirstName());
        assertEquals("alavi", result.getApartemantList().get(0).getOwner().getLastName());
        assertEquals("091234567", result.getApartemantList().get(0).getOwner().getMobileNumber());


        assertEquals("ali", result.getApartemantList().get(0).getRoomer().getFirstName());
        assertEquals("alavi", result.getApartemantList().get(0).getRoomer().getLastName());
        assertEquals("091234567", result.getApartemantList().get(0).getRoomer().getMobileNumber());

        assertEquals("w", result.getApartemantList().get(0).getLocation());
        assertEquals(2000, result.getApartemantList().get(0).getRimanIn());
        assertEquals(100, result.getApartemantList().get(0).getSpace());
        assertEquals(2, result.getApartemantList().get(0).getPersonNumber());
        assertEquals(200000, result.getApartemantList().get(0).getCharge());

        assertEquals("ali", result.getApartemantList().get(1).getOwner().getFirstName());
        assertEquals("alavi", result.getApartemantList().get(1).getOwner().getLastName());
        assertEquals("091234567", result.getApartemantList().get(1).getOwner().getMobileNumber());

        assertEquals("abas", result.getApartemantList().get(1).getRoomer().getFirstName());
        assertEquals("asadi", result.getApartemantList().get(1).getRoomer().getLastName());
        assertEquals("09123400", result.getApartemantList().get(1).getRoomer().getMobileNumber());

        assertEquals("s", result.getApartemantList().get(1).getLocation());
        assertEquals(3000, result.getApartemantList().get(1).getRimanIn());
        assertEquals(200, result.getApartemantList().get(1).getSpace());
        assertEquals(3, result.getApartemantList().get(1).getPersonNumber());
        assertEquals(200000, result.getApartemantList().get(1).getCharge());

        assertEquals(543, result.getHouseAccount().getAccountNumber());
        assertEquals("meli", result.getHouseAccount().getNameBank());
        assertEquals(876, result.getHouseAccount().getCardNumber());

        List<House> houseList = repository.findAll();

        assertEquals("abi", houseList.get(0).getHouseName());
        assertEquals(20000, houseList.get(0).getRemain());
        assertEquals(2, houseList.get(0).getNumber());

        assertEquals("thran", houseList.get(0).getAdress().getState());
        assertEquals("thran", houseList.get(0).getAdress().getCity());
        assertEquals("serag", houseList.get(0).getAdress().getArea());
        assertEquals("]eraghi", houseList.get(0).getAdress().getPostAdress());
        assertEquals("123456", houseList.get(0).getAdress().getPostCode());

        assertEquals("meli", houseList.get(0).getHouseAccount().getNameBank());
        assertEquals(543, houseList.get(0).getHouseAccount().getAccountNumber());
        assertEquals(876, houseList.get(0).getHouseAccount().getCardNumber());

        assertEquals("ali", houseList.get(0).getMangerList().get(0).getFirstName());
        assertEquals("alavi", houseList.get(0).getMangerList().get(0).getLastName());
        assertEquals("091234567", houseList.get(0).getMangerList().get(0).getMobileNumber());


        assertEquals("hasan", houseList.get(0).getMangerList().get(1).getFirstName());
        assertEquals("hasani", houseList.get(0).getMangerList().get(1).getLastName());
        assertEquals("091234444", houseList.get(0).getMangerList().get(1).getMobileNumber());


        assertEquals("ali", houseList.get(0).getApartemantList().get(0).getOwner().getFirstName());
        assertEquals("alavi", houseList.get(0).getApartemantList().get(0).getOwner().getLastName());
        assertEquals("091234567", houseList.get(0).getApartemantList().get(0).getOwner().getMobileNumber());


        assertEquals("ali", houseList.get(0).getApartemantList().get(0).getRoomer().getFirstName());
        assertEquals("alavi", houseList.get(0).getApartemantList().get(0).getRoomer().getLastName());
        assertEquals("091234567", houseList.get(0).getApartemantList().get(0).getRoomer().getMobileNumber());

        assertEquals("w", houseList.get(0).getApartemantList().get(0).getLocation());
        assertEquals(2000, houseList.get(0).getApartemantList().get(0).getRimanIn());
        assertEquals(100, houseList.get(0).getApartemantList().get(0).getSpace());
        assertEquals(2, houseList.get(0).getApartemantList().get(0).getPersonNumber());
        assertEquals(200000, houseList.get(0).getApartemantList().get(0).getCharge());

        assertEquals("ali", houseList.get(0).getApartemantList().get(1).getOwner().getFirstName());
        assertEquals("alavi", houseList.get(0).getApartemantList().get(1).getOwner().getLastName());
        assertEquals("091234567", houseList.get(0).getApartemantList().get(1).getOwner().getMobileNumber());

        assertEquals("abas", houseList.get(0).getApartemantList().get(1).getRoomer().getFirstName());
        assertEquals("asadi", houseList.get(0).getApartemantList().get(1).getRoomer().getLastName());
        assertEquals("09123400", houseList.get(0).getApartemantList().get(1).getRoomer().getMobileNumber());

        assertEquals("s", houseList.get(0).getApartemantList().get(1).getLocation());
        assertEquals(3000, houseList.get(0).getApartemantList().get(1).getRimanIn());
        assertEquals(200, houseList.get(0).getApartemantList().get(1).getSpace());
        assertEquals(3, houseList.get(0).getApartemantList().get(1).getPersonNumber());
        assertEquals(200000, houseList.get(0).getApartemantList().get(1).getCharge());

        assertEquals(543, houseList.get(0).getHouseAccount().getAccountNumber());
        assertEquals("meli", houseList.get(0).getHouseAccount().getNameBank());
        assertEquals(876, houseList.get(0).getHouseAccount().getCardNumber());
    }

    @Test
    public void retrieveHouse() {
        Adress adress = new Adress("thran", "thran", "serag", "]eraghi", "123456");
        HouseManeger maneger1 = new HouseManeger("ali", "alavi", "091234567");
        HouseManeger maneger2 = new HouseManeger("hasan", "hasani", "091234444");
        List<HouseManeger> manegerList = new ArrayList<HouseManeger>();
        manegerList.add(maneger1);
        manegerList.add(maneger2);
        Owner owner = new Owner("ali", "alavi", "091234567");
        Roomer roomer = new Roomer("ali", "alavi", "091234567");
        Apartemant apartemant1 = new Apartemant(roomer, owner, "w", 2000, 100, 2, 200000);

        Owner owner1 = new Owner("ali", "alavi", "091234567");
        Roomer roomer1 = new Roomer("abas", "asadi", "09123400");
        Apartemant apartemant2 = new Apartemant(roomer1, owner1, "s", 3000, 200, 3, 200000);
        List<Apartemant> apartemantList = new ArrayList<Apartemant>();
        apartemantList.add(apartemant1);
        apartemantList.add(apartemant2);
        HouseAccount account = new HouseAccount("meli", 543, 876);
        House house = new House("abi", 20000, adress, account, manegerList, apartemantList);
        String time = new SimpleDateFormat("yyyy.MM.dd  HH:mm:ss").format(Calendar.getInstance().getTime());

        House result = repository.save(house);
        Log log = new Log(" find house by" + result.getId() + "id", time);
        Log log1 = logRepository.save(log);
        assertEquals(" find house by" + result.getId() + "id", log1.getStatus());
        assertEquals(time, log1.getTime());

        assertEquals("abi", result.getHouseName());
        assertEquals(20000, result.getRemain());
        assertEquals(2, result.getNumber());

        assertEquals("thran", result.getAdress().getState());
        assertEquals("thran", result.getAdress().getCity());
        assertEquals("serag", result.getAdress().getArea());
        assertEquals("]eraghi", result.getAdress().getPostAdress());
        assertEquals("123456", result.getAdress().getPostCode());

        assertEquals("meli", result.getHouseAccount().getNameBank());
        assertEquals(543, result.getHouseAccount().getAccountNumber());
        assertEquals(876, result.getHouseAccount().getCardNumber());

        assertEquals("ali", result.getMangerList().get(0).getFirstName());
        assertEquals("alavi", result.getMangerList().get(0).getLastName());
        assertEquals("091234567", result.getMangerList().get(0).getMobileNumber());


        assertEquals("hasan", result.getMangerList().get(1).getFirstName());
        assertEquals("hasani", result.getMangerList().get(1).getLastName());
        assertEquals("091234444", result.getMangerList().get(1).getMobileNumber());


        assertEquals("ali", result.getApartemantList().get(0).getOwner().getFirstName());
        assertEquals("alavi", result.getApartemantList().get(0).getOwner().getLastName());
        assertEquals("091234567", result.getApartemantList().get(0).getOwner().getMobileNumber());


        assertEquals("ali", result.getApartemantList().get(0).getRoomer().getFirstName());
        assertEquals("alavi", result.getApartemantList().get(0).getRoomer().getLastName());
        assertEquals("091234567", result.getApartemantList().get(0).getRoomer().getMobileNumber());

        assertEquals("w", result.getApartemantList().get(0).getLocation());
        assertEquals(2000, result.getApartemantList().get(0).getRimanIn());
        assertEquals(100, result.getApartemantList().get(0).getSpace());
        assertEquals(2, result.getApartemantList().get(0).getPersonNumber());
        assertEquals(200000, result.getApartemantList().get(0).getCharge());

        assertEquals("ali", result.getApartemantList().get(1).getOwner().getFirstName());
        assertEquals("alavi", result.getApartemantList().get(1).getOwner().getLastName());
        assertEquals("091234567", result.getApartemantList().get(1).getOwner().getMobileNumber());

        assertEquals("abas", result.getApartemantList().get(1).getRoomer().getFirstName());
        assertEquals("asadi", result.getApartemantList().get(1).getRoomer().getLastName());
        assertEquals("09123400", result.getApartemantList().get(1).getRoomer().getMobileNumber());

        assertEquals("s", result.getApartemantList().get(1).getLocation());
        assertEquals(3000, result.getApartemantList().get(1).getRimanIn());
        assertEquals(200, result.getApartemantList().get(1).getSpace());
        assertEquals(3, result.getApartemantList().get(1).getPersonNumber());
        assertEquals(200000, result.getApartemantList().get(1).getCharge());

        assertEquals(543, result.getHouseAccount().getAccountNumber());
        assertEquals("meli", result.getHouseAccount().getNameBank());
        assertEquals(876, result.getHouseAccount().getCardNumber());
        long id = result.getId();
        Optional<House> house1 = repository.findById(id);
        assertTrue(house1.isPresent());

        assertEquals(house, house1.get());
    }

    @Test
    public void deleteHouse() {
        Adress adress = new Adress("thran", "thran", "serag", "]eraghi", "123456");
        HouseManeger maneger1 = new HouseManeger("ali", "alavi", "091234567");
        HouseManeger maneger2 = new HouseManeger("hasan", "hasani", "091234444");
        List<HouseManeger> manegerList = new ArrayList<HouseManeger>();
        manegerList.add(maneger1);
        manegerList.add(maneger2);
        Owner owner = new Owner("ali", "alavi", "091234567");
        Roomer roomer = new Roomer("ali", "alavi", "091234567");
        Apartemant apartemant1 = new Apartemant(roomer, owner, "w", 2000, 100, 2, 200000);

        Owner owner1 = new Owner("ali", "alavi", "091234567");
        Roomer roomer1 = new Roomer("abas", "asadi", "09123400");
        Apartemant apartemant2 = new Apartemant(roomer1, owner1, "s", 3000, 200, 3, 200000);
        List<Apartemant> apartemantList = new ArrayList<Apartemant>();
        apartemantList.add(apartemant1);
        apartemantList.add(apartemant2);
        HouseAccount account = new HouseAccount("meli", 543, 876);
        House house = new House("abi", 20000, adress, account, manegerList, apartemantList);
        House result = repository.save(house);
        String time = new SimpleDateFormat("yyyy.MM.dd  HH:mm:ss").format(Calendar.getInstance().getTime());
        Log log = new Log("delete house by" + result.getId() + "id", time);
        Log log1 = logRepository.save(log);
        assertEquals("delete house by" + result.getId() + "id", log1.getStatus());
        assertEquals(time, log1.getTime());

        assertEquals("abi", result.getHouseName());
        assertEquals(20000, result.getRemain());
        assertEquals(2, result.getNumber());

        assertEquals("thran", result.getAdress().getState());
        assertEquals("thran", result.getAdress().getCity());
        assertEquals("serag", result.getAdress().getArea());
        assertEquals("]eraghi", result.getAdress().getPostAdress());
        assertEquals("123456", result.getAdress().getPostCode());

        assertEquals("meli", result.getHouseAccount().getNameBank());
        assertEquals(543, result.getHouseAccount().getAccountNumber());
        assertEquals(876, result.getHouseAccount().getCardNumber());

        assertEquals("ali", result.getMangerList().get(0).getFirstName());
        assertEquals("alavi", result.getMangerList().get(0).getLastName());
        assertEquals("091234567", result.getMangerList().get(0).getMobileNumber());


        assertEquals("hasan", result.getMangerList().get(1).getFirstName());
        assertEquals("hasani", result.getMangerList().get(1).getLastName());
        assertEquals("091234444", result.getMangerList().get(1).getMobileNumber());


        assertEquals("ali", result.getApartemantList().get(0).getOwner().getFirstName());
        assertEquals("alavi", result.getApartemantList().get(0).getOwner().getLastName());
        assertEquals("091234567", result.getApartemantList().get(0).getOwner().getMobileNumber());


        assertEquals("ali", result.getApartemantList().get(0).getRoomer().getFirstName());
        assertEquals("alavi", result.getApartemantList().get(0).getRoomer().getLastName());
        assertEquals("091234567", result.getApartemantList().get(0).getRoomer().getMobileNumber());

        assertEquals("w", result.getApartemantList().get(0).getLocation());
        assertEquals(2000, result.getApartemantList().get(0).getRimanIn());
        assertEquals(100, result.getApartemantList().get(0).getSpace());
        assertEquals(2, result.getApartemantList().get(0).getPersonNumber());
        assertEquals(200000, result.getApartemantList().get(0).getCharge());

        assertEquals("ali", result.getApartemantList().get(1).getOwner().getFirstName());
        assertEquals("alavi", result.getApartemantList().get(1).getOwner().getLastName());
        assertEquals("091234567", result.getApartemantList().get(1).getOwner().getMobileNumber());

        assertEquals("abas", result.getApartemantList().get(1).getRoomer().getFirstName());
        assertEquals("asadi", result.getApartemantList().get(1).getRoomer().getLastName());
        assertEquals("09123400", result.getApartemantList().get(1).getRoomer().getMobileNumber());

        assertEquals("s", result.getApartemantList().get(1).getLocation());
        assertEquals(3000, result.getApartemantList().get(1).getRimanIn());
        assertEquals(200, result.getApartemantList().get(1).getSpace());
        assertEquals(3, result.getApartemantList().get(1).getPersonNumber());
        assertEquals(200000, result.getApartemantList().get(1).getCharge());

        assertEquals(543, result.getHouseAccount().getAccountNumber());
        assertEquals("meli", result.getHouseAccount().getNameBank());
        assertEquals(876, result.getHouseAccount().getCardNumber());
        long id = result.getId();

        repository.deleteById(result.getId());
        Optional<House> house1 = repository.findById(id);
        assertFalse(house1.isPresent());
        assertNotEquals(house, house1);
    }

    @Test
    public void createHouse() {
        Adress adress = new Adress("thran", "thran", "serag", "]eraghi", "123456");
        HouseManeger maneger1 = new HouseManeger("ali", "alavi", "091234567");
        HouseManeger maneger2 = new HouseManeger("hasan", "hasani", "091234444");
        List<HouseManeger> manegerList = new ArrayList<HouseManeger>();
        manegerList.add(maneger1);
        manegerList.add(maneger2);
        Owner owner = new Owner("ali", "alavi", "091234567");
        Roomer roomer = new Roomer("ali", "alavi", "091234567");
        Apartemant apartemant1 = new Apartemant(roomer, owner, "w", 2000, 100, 2, 200000);

        Owner owner1 = new Owner("ali", "alavi", "091234567");
        Roomer roomer1 = new Roomer("abas", "asadi", "09123400");
        Apartemant apartemant2 = new Apartemant(roomer1, owner1, "s", 3000, 200, 3, 200000);
        List<Apartemant> apartemantList = new ArrayList<Apartemant>();
        apartemantList.add(apartemant1);
        apartemantList.add(apartemant2);
        HouseAccount account = new HouseAccount("meli", 543, 876);
        House house = new House("abi", 20000, adress, account, manegerList, apartemantList);

        House result = repository.save(house);
        String time = new SimpleDateFormat("yyyy.MM.dd  HH:mm:ss").format(Calendar.getInstance().getTime());
        Log log = new Log("create house by" + result.getId() + "id", time);
        Log log1 = logRepository.save(log);
        assertEquals("create house by" + result.getId() + "id", log1.getStatus());
        assertEquals(time, log1.getTime());

        assertEquals("abi", result.getHouseName());
        assertEquals(20000, result.getRemain());
        assertEquals(2, result.getNumber());

        assertEquals("thran", result.getAdress().getState());
        assertEquals("thran", result.getAdress().getCity());
        assertEquals("serag", result.getAdress().getArea());
        assertEquals("]eraghi", result.getAdress().getPostAdress());
        assertEquals("123456", result.getAdress().getPostCode());

        assertEquals("meli", result.getHouseAccount().getNameBank());
        assertEquals(543, result.getHouseAccount().getAccountNumber());
        assertEquals(876, result.getHouseAccount().getCardNumber());

        assertEquals("ali", result.getMangerList().get(0).getFirstName());
        assertEquals("alavi", result.getMangerList().get(0).getLastName());
        assertEquals("091234567", result.getMangerList().get(0).getMobileNumber());


        assertEquals("hasan", result.getMangerList().get(1).getFirstName());
        assertEquals("hasani", result.getMangerList().get(1).getLastName());
        assertEquals("091234444", result.getMangerList().get(1).getMobileNumber());


        assertEquals("ali", result.getApartemantList().get(0).getOwner().getFirstName());
        assertEquals("alavi", result.getApartemantList().get(0).getOwner().getLastName());
        assertEquals("091234567", result.getApartemantList().get(0).getOwner().getMobileNumber());


        assertEquals("ali", result.getApartemantList().get(0).getRoomer().getFirstName());
        assertEquals("alavi", result.getApartemantList().get(0).getRoomer().getLastName());
        assertEquals("091234567", result.getApartemantList().get(0).getRoomer().getMobileNumber());

        assertEquals("w", result.getApartemantList().get(0).getLocation());
        assertEquals(2000, result.getApartemantList().get(0).getRimanIn());
        assertEquals(100, result.getApartemantList().get(0).getSpace());
        assertEquals(2, result.getApartemantList().get(0).getPersonNumber());
        assertEquals(200000, result.getApartemantList().get(0).getCharge());


        assertEquals("ali", result.getApartemantList().get(1).getOwner().getFirstName());
        assertEquals("alavi", result.getApartemantList().get(1).getOwner().getLastName());
        assertEquals("091234567", result.getApartemantList().get(1).getOwner().getMobileNumber());


        assertEquals("abas", result.getApartemantList().get(1).getRoomer().getFirstName());
        assertEquals("asadi", result.getApartemantList().get(1).getRoomer().getLastName());
        assertEquals("09123400", result.getApartemantList().get(1).getRoomer().getMobileNumber());

        assertEquals("s", result.getApartemantList().get(1).getLocation());
        assertEquals(3000, result.getApartemantList().get(1).getRimanIn());
        assertEquals(200, result.getApartemantList().get(1).getSpace());
        assertEquals(3, result.getApartemantList().get(1).getPersonNumber());
        assertEquals(200000, result.getApartemantList().get(1).getCharge());

        assertEquals(543, result.getHouseAccount().getAccountNumber());
        assertEquals("meli", result.getHouseAccount().getNameBank());
        assertEquals(876, result.getHouseAccount().getCardNumber());
    }

    @Test
    public void updateHouse() {
        Adress adress = new Adress("thran", "thran", "serag", "]eraghi", "123456");
        HouseManeger maneger1 = new HouseManeger("ali", "alavi", "091234567");
        HouseManeger maneger2 = new HouseManeger("hasan", "hasani", "091234444");
        List<HouseManeger> manegerList = new ArrayList<HouseManeger>();
        manegerList.add(maneger1);
        manegerList.add(maneger2);
        Owner owner = new Owner("ali", "alavi", "091234567");
        Roomer roomer = new Roomer("ali", "alavi", "091234567");
        Apartemant apartemant1 = new Apartemant(roomer, owner, "w", 2000, 100, 2, 200000);

        Owner owner1 = new Owner("ali", "alavi", "091234567");
        Roomer roomer1 = new Roomer("abas", "asadi", "09123400");
        Apartemant apartemant2 = new Apartemant(roomer1, owner1, "s", 3000, 200, 3, 200000);
        List<Apartemant> apartemantList = new ArrayList<Apartemant>();
        apartemantList.add(apartemant1);
        apartemantList.add(apartemant2);
        HouseAccount account = new HouseAccount("meli", 543, 876);
        House house = new House("abi", 20000, adress, account, manegerList, apartemantList);
        House result = repository.save(house);
        result.setHouseName("sabz");
        House result1 = repository.save(result);
        String time = new SimpleDateFormat("yyyy.MM.dd  HH:mm:ss").format(Calendar.getInstance().getTime());
        Log log = new Log("update name house by" + result1.getId() + "id", time);
        Log log1 = logRepository.save(log);
        assertEquals("update name house by" + result1.getId() + "id", log1.getStatus());
        assertEquals(time, log1.getTime());

        assertEquals("sabz", result1.getHouseName());
    }

    @Test
    public void retrieveLog() {
        String time = new SimpleDateFormat("yyyy.MM.dd  HH:mm:ss").format(Calendar.getInstance().getTime());
        Log log = new Log(" find all houses", time);
        Log log1 = logRepository.save(log);
        assertEquals(" find all houses", log1.getStatus());
        assertEquals(time, log1.getTime());
        Optional<Log> logOptional = logRepository.findById(log1.getId());
        assertTrue(logOptional.isPresent());
        assertEquals(log, logOptional.get());
    }
}