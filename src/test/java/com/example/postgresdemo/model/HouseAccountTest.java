package com.example.postgresdemo.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class HouseAccountTest {

    @Test
    public void testHouseAccount() {
        HouseAccount account = new HouseAccount();
        assertEquals(0, account.getId());
        assertEquals(0, account.getAccountNumber());
        assertEquals(0, account.getCardNumber());
        assertEquals(null, account.getNameBank());
    }

    @Test
    public void testHouseAccountLongStringLongLong() {
        //fail("Not yet implemented");
        HouseAccount account = new HouseAccount("meli", 543, 876);

        assertEquals(0, account.getId());
        assertEquals("meli", account.getNameBank());
        assertEquals(543, account.getAccountNumber());
        assertEquals(876, account.getCardNumber());
    }

    @Test
    public void testGetId() {
        //fail("Not yet implemented");
        HouseAccount account = new HouseAccount();
        account.setId(1);

        assertEquals(1, account.getId());
    }

    @Test
    public void testSetId() {
        //fail("Not yet implemented");
        HouseAccount account = new HouseAccount();
        account.setId(2);

        assertEquals(2, account.getId());
    }

    @Test
    public void testGetNameBank() {
        //fail("Not yet implemented");
        HouseAccount account = new HouseAccount("meli", 543, 876);
        assertEquals("meli", account.getNameBank());
    }

    @Test
    public void testSetNameBank() {
        //fail("Not yet implemented");
        HouseAccount account = new HouseAccount();
        account.setNameBank("melat");

        assertEquals("melat", account.getNameBank());

    }

    @Test
    public void testGetAccountNumber() {
        //fail("Not yet implemented");
        HouseAccount account = new HouseAccount("meli", 543, 876);
        assertEquals(543, account.getAccountNumber());

    }

    @Test
    public void testSetAccountNumber() {
        //fail("Not yet implemented");
        HouseAccount account = new HouseAccount();
        account.setAccountNumber(345);
        assertEquals(345, account.getAccountNumber());
    }

    @Test
    public void testGetCardNumber() {
        HouseAccount account = new HouseAccount("meli", 543, 876);
        assertEquals(876, account.getCardNumber());
    }

    @Test
    public void testSetCardNumber() {
        HouseAccount account = new HouseAccount();
        account.setCardNumber(765);
        assertEquals(765, account.getCardNumber());
    }

}