package com.example.postgresdemo.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class HouseManegerTest {

    @Test
    public void testHouseManeger() {
        HouseManeger maneger = new HouseManeger();
        assertEquals(0, maneger.getId());
        assertEquals(null, maneger.getFirstName());
        assertEquals(null, maneger.getLastName());
        assertEquals(null, maneger.getMobileNumber());
    }

    @Test
    public void testHouseManegerLongStringStringString() {
        HouseManeger maneger = new HouseManeger("ali", "alavi", "091234567");
        assertEquals(0, maneger.getId());
        assertEquals("ali", maneger.getFirstName());
        assertEquals("alavi", maneger.getLastName());
        assertEquals("091234567", maneger.getMobileNumber());
    }

    @Test
    public void testGetId() {
        //fail("Not yet implemented");
        HouseManeger maneger = new HouseManeger();
        maneger.setId(2);
        assertEquals(2, maneger.getId());
    }

    @Test
    public void testSetId() {
        //fail("Not yet implemented");
        HouseManeger maneger = new HouseManeger();
        maneger.setId(3);
        assertEquals(3, maneger.getId());
    }

    @Test
    public void testGetFirstName() {
        //fail("Not yet implemented");
        HouseManeger maneger = new HouseManeger("ali", "alavi", "091234567");
        assertEquals("ali", maneger.getFirstName());
    }

    @Test
    public void testSetFirstName() {
        //fail("Not yet implemented");
        HouseManeger maneger = new HouseManeger();
        maneger.setFirstName("hasan");
        assertEquals("hasan", maneger.getFirstName());
    }

    @Test
    public void testGetLastName() {
        //fail("Not yet implemented");
        HouseManeger maneger = new HouseManeger("ali", "alavi", "091234567");
        assertEquals("alavi", maneger.getLastName());
    }

    @Test
    public void testSetLastName() {
        //fail("Not yet implemented");
        HouseManeger maneger = new HouseManeger();
        maneger.setLastName("farhadi");
        assertEquals("farhadi", maneger.getLastName());
    }

    @Test
    public void testGetMobileNumber() {
        HouseManeger maneger = new HouseManeger("ali", "alavi", "091234567");
        assertEquals("091234567", maneger.getMobileNumber());
    }

    @Test
    public void testSetMobileNumber() {
        HouseManeger maneger = new HouseManeger();
        maneger.setMobileNumber("09123456");
        assertEquals("09123456", maneger.getMobileNumber());
    }

}