package com.example.postgresdemo.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class OwnerTest {

    @Test
    public void testOwner() {
        Owner owner = new Owner();
        assertEquals(null, owner.getFirstName());
        assertEquals(null, owner.getLastName());
        assertEquals(null, owner.getMobileNumber());
    }

    @Test
    public void testOwnerLongStringStringString() {
        //fail("Not yet implemented");
        Owner owner = new Owner("ali", "alavi", "091234567");
        assertEquals("ali", owner.getFirstName());
        assertEquals("alavi", owner.getLastName());
        assertEquals("091234567", owner.getMobileNumber());
    }

    @Test
    public void testGetFirstName() {
        //fail("Not yet implemented");
        Owner owner = new Owner("ali", "alavi", "091234567");
        assertEquals("ali", owner.getFirstName());
    }

    @Test
    public void testSetFirstName() {
        //fail("Not yet implemented");
        Owner owner = new Owner();
        owner.setFirstName("hasan");
        assertEquals("hasan", owner.getFirstName());
    }

    @Test
    public void testGetLastName() {
        Owner owner = new Owner("ali", "alavi", "091234567");

        assertEquals("alavi", owner.getLastName());
    }

    @Test
    public void testSetLastName() {
        //fail("Not yet implemented");
        Owner owner = new Owner();
        owner.setLastName("mosavi");
        assertEquals("mosavi", owner.getLastName());
    }

    @Test
    public void testGetMobileNumber() {
        //fail("Not yet implemented");
        Owner owner = new Owner("ali", "alavi", "091234567");

        assertEquals("091234567", owner.getMobileNumber());
    }

    @Test
    public void testSetMobileNumber() {
        //fail("Not yet implemented");
        Owner owner = new Owner();
        owner.setMobileNumber("091287655");
        assertEquals("091287655", owner.getMobileNumber());
    }

}