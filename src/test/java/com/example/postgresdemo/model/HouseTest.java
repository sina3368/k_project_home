package com.example.postgresdemo.model;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class HouseTest {
    House house;

    @Before
    public void setUp() throws Exception {
        Adress adress = new Adress("thran", "thran", "serag", "]eraghi", "123456");
        HouseManeger maneger1 = new HouseManeger("ali", "alavi", "091234567");
        HouseManeger maneger2 = new HouseManeger("hasan", "hasani", "091234444");
        List<HouseManeger> manegerList = new ArrayList<HouseManeger>();
        manegerList.add(maneger1);
        manegerList.add(maneger2);
        Owner owner = new Owner("ali", "alavi", "091234567");
        Roomer roomer = new Roomer("ali", "alavi", "091234567");
        Apartemant apartemant1 = new Apartemant(roomer, owner, "w", 2000, 100, 2, 200000);

        Owner owner1 = new Owner("ali", "alavi", "091234567");
        Roomer roomer1 = new Roomer("abas", "asadi", "09123400");
        Apartemant apartemant2 = new Apartemant(roomer1, owner1, "s", 3000, 200, 3, 200000);
        List<Apartemant> apartemantList = new ArrayList<Apartemant>();
        apartemantList.add(apartemant1);
        apartemantList.add(apartemant2);
        HouseAccount account = new HouseAccount("meli", 543, 876);
        house = new House("abi", 20000, adress, account, manegerList, apartemantList);

    }

    @Test
    public void testHouse() {
        House house1 = new House();
        assertEquals(0, house1.getId());
        assertEquals(null, house1.getAdress());
        assertEquals(0, house1.getRemain());
        assertEquals(null, house1.getHouseName());
        assertEquals(null, house1.getHouseAccount());
        assertEquals(null, house1.getMangerList());
        assertEquals(0, house1.getNumber());
        assertEquals(null, house1.getApartemantList());
    }

    @Test
    public void testHouseStringLongAdressHouseAccountListOfHouseManegerListOfApartemant() {
        assertEquals(0, house.getId());
        assertEquals("abi", house.getHouseName());
        assertEquals(20000, house.getRemain());
        assertEquals(2, house.getNumber());

        assertEquals(0, house.getAdress().getId());
        assertEquals("thran", house.getAdress().getState());
        assertEquals("thran", house.getAdress().getCity());
        assertEquals("serag", house.getAdress().getArea());
        assertEquals("]eraghi", house.getAdress().getPostAdress());
        assertEquals("123456", house.getAdress().getPostCode());

        assertEquals(0, house.getHouseAccount().getId());
        assertEquals("meli", house.getHouseAccount().getNameBank());
        assertEquals(543, house.getHouseAccount().getAccountNumber());
        assertEquals(876, house.getHouseAccount().getCardNumber());

        assertEquals(0, house.getMangerList().get(0).getId());
        assertEquals("ali", house.getMangerList().get(0).getFirstName());
        assertEquals("alavi", house.getMangerList().get(0).getLastName());
        assertEquals("091234567", house.getMangerList().get(0).getMobileNumber());

        assertEquals(0, house.getMangerList().get(1).getId());
        assertEquals("hasan", house.getMangerList().get(1).getFirstName());
        assertEquals("hasani", house.getMangerList().get(1).getLastName());
        assertEquals("091234444", house.getMangerList().get(1).getMobileNumber());

        assertEquals(0, house.getApartemantList().get(0).getId());

        assertEquals("ali", house.getApartemantList().get(0).getOwner().getFirstName());
        assertEquals("alavi", house.getApartemantList().get(0).getOwner().getLastName());
        assertEquals("091234567", house.getApartemantList().get(0).getOwner().getMobileNumber());

        assertEquals("ali", house.getApartemantList().get(0).getRoomer().getFirstName());
        assertEquals("alavi", house.getApartemantList().get(0).getRoomer().getLastName());
        assertEquals("091234567", house.getApartemantList().get(0).getRoomer().getMobileNumber());

        assertEquals("w", house.getApartemantList().get(0).getLocation());
        assertEquals(2000, house.getApartemantList().get(0).getRimanIn());
        assertEquals(100, house.getApartemantList().get(0).getSpace());
        assertEquals(2, house.getApartemantList().get(0).getPersonNumber());
        assertEquals(200000, house.getApartemantList().get(0).getCharge());

        assertEquals(0, house.getApartemantList().get(1).getId());

        assertEquals("ali", house.getApartemantList().get(1).getOwner().getFirstName());
        assertEquals("alavi", house.getApartemantList().get(1).getOwner().getLastName());
        assertEquals("091234567", house.getApartemantList().get(1).getOwner().getMobileNumber());

        assertEquals("abas", house.getApartemantList().get(1).getRoomer().getFirstName());
        assertEquals("asadi", house.getApartemantList().get(1).getRoomer().getLastName());
        assertEquals("09123400", house.getApartemantList().get(1).getRoomer().getMobileNumber());

        assertEquals("s", house.getApartemantList().get(1).getLocation());
        assertEquals(3000, house.getApartemantList().get(1).getRimanIn());
        assertEquals(200, house.getApartemantList().get(1).getSpace());
        assertEquals(3, house.getApartemantList().get(1).getPersonNumber());
        assertEquals(200000, house.getApartemantList().get(1).getCharge());

        assertEquals(0, house.getHouseAccount().getId());
        assertEquals(543, house.getHouseAccount().getAccountNumber());
        assertEquals("meli", house.getHouseAccount().getNameBank());
        assertEquals(876, house.getHouseAccount().getCardNumber());


    }

    @Test
    public void testGetId() {
        // fail("Not yet implemented");
        House house1 = new House();
        house1.setId(1);

        assertEquals(1, house1.getId());
    }

    @Test
    public void testSetId() {
        // fail("Not yet implemented");
        House house1 = new House();
        house1.setId(2);

        assertEquals(2, house1.getId());
    }

    @Test
    public void testGetHouseName() {
        //fail("Not yet implemented");
        assertEquals("abi", house.getHouseName());
    }

    @Test
    public void testSetHouseName() {
        // fail("Not yet implemented");
        House house1 = new House();
        house1.setHouseName("sabz");

        assertEquals("sabz", house1.getHouseName());
    }

    @Test
    public void testGetRemain() {
        //fail("Not yet implemented");
        assertEquals(20000, house.getRemain());

    }

    @Test
    public void testSetRemain() {
        // fail("Not yet implemented");
        House house1 = new House();
        house1.setRemain(234);

        assertEquals(234, house1.getRemain());
    }

    @Test
    public void testGetAdress() {
        //fail("Not yet implemented");
        assertEquals(0, house.getAdress().getId());
        assertEquals("thran", house.getAdress().getState());
        assertEquals("thran", house.getAdress().getCity());
        assertEquals("serag", house.getAdress().getArea());
        assertEquals("]eraghi", house.getAdress().getPostAdress());
        assertEquals("123456", house.getAdress().getPostCode());
    }

    @Test
    public void testSetAdress() {
        // fail("Not yet implemented");
        Adress adress = new Adress("Azarbayjan", "tabriz", "markazi", "aval", "1234565");
        House house1 = new House();
        house1.setAdress(adress);
        assertEquals(0, house1.getAdress().getId());
        assertEquals("Azarbayjan", house1.getAdress().getState());
        assertEquals("tabriz", house1.getAdress().getCity());
        assertEquals("markazi", house1.getAdress().getArea());
        assertEquals("aval", house1.getAdress().getPostAdress());
        assertEquals("1234565", house1.getAdress().getPostCode());
    }

    @Test
    public void testGetHouseAccount() {
        // fail("Not yet implemented");
        assertEquals(0, house.getHouseAccount().getId());
        assertEquals("meli", house.getHouseAccount().getNameBank());
        assertEquals(543, house.getHouseAccount().getAccountNumber());
        assertEquals(876, house.getHouseAccount().getCardNumber());
    }

    @Test
    public void testSetHouseAccount() {
        // fail("Not yet implemented");
        HouseAccount account = new HouseAccount("meli", 5434, 1876);
        House house1 = new House();
        house1.setHouseAccount(account);
        assertEquals(0, house1.getHouseAccount().getId());
        assertEquals("meli", house1.getHouseAccount().getNameBank());
        assertEquals(5434, house1.getHouseAccount().getAccountNumber());
        assertEquals(1876, house1.getHouseAccount().getCardNumber());
    }

    @Test
    public void testGetMangerList() {
        //fail("Not yet implemented");
        assertEquals(0, house.getMangerList().get(0).getId());
        assertEquals("ali", house.getMangerList().get(0).getFirstName());
        assertEquals("alavi", house.getMangerList().get(0).getLastName());
        assertEquals("091234567", house.getMangerList().get(0).getMobileNumber());

        assertEquals(0, house.getMangerList().get(1).getId());
        assertEquals("hasan", house.getMangerList().get(1).getFirstName());
        assertEquals("hasani", house.getMangerList().get(1).getLastName());
        assertEquals("091234444", house.getMangerList().get(1).getMobileNumber());
    }

    @Test
    public void testSetMangerList() {
        // fail("Not yet implemented");
        HouseManeger maneger1 = new HouseManeger("ali", "asdi", "091234567");
        HouseManeger maneger2 = new HouseManeger("hasan", "baboli", "091234444");
        List<HouseManeger> manegerList = new ArrayList<HouseManeger>();
        manegerList.add(maneger1);
        manegerList.add(maneger2);
        House house1 = new House();
        house1.setMangerList(manegerList);

        assertEquals(0, house1.getMangerList().get(0).getId());
        assertEquals("ali", house1.getMangerList().get(0).getFirstName());
        assertEquals("asdi", house1.getMangerList().get(0).getLastName());
        assertEquals("091234567", house1.getMangerList().get(0).getMobileNumber());

        assertEquals(0, house1.getMangerList().get(1).getId());
        assertEquals("hasan", house1.getMangerList().get(1).getFirstName());
        assertEquals("baboli", house1.getMangerList().get(1).getLastName());
        assertEquals("091234444", house1.getMangerList().get(1).getMobileNumber());
    }

    @Test
    public void testGetNumber() {
        // fail("Not yet implemented");
        assertEquals(2, house.getNumber());
    }

    @Test
    public void testSetNumber() {
        // fail("Not yet implemented");
        House house1 = new House();
        house1.setNumber(4);
        assertEquals(4, house1.getNumber());
    }

    @Test
    public void testGetApartemantList() {
        //fail("Not yet implemented");
        assertEquals(0, house.getApartemantList().get(0).getId());

        assertEquals("ali", house.getApartemantList().get(0).getOwner().getFirstName());
        assertEquals("alavi", house.getApartemantList().get(0).getOwner().getLastName());
        assertEquals("091234567", house.getApartemantList().get(0).getOwner().getMobileNumber());

        assertEquals("ali", house.getApartemantList().get(0).getRoomer().getFirstName());
        assertEquals("alavi", house.getApartemantList().get(0).getRoomer().getLastName());
        assertEquals("091234567", house.getApartemantList().get(0).getRoomer().getMobileNumber());

        assertEquals("w", house.getApartemantList().get(0).getLocation());
        assertEquals(2000, house.getApartemantList().get(0).getRimanIn());
        assertEquals(100, house.getApartemantList().get(0).getSpace());
        assertEquals(2, house.getApartemantList().get(0).getPersonNumber());
        assertEquals(200000, house.getApartemantList().get(0).getCharge());

        assertEquals(0, house.getApartemantList().get(1).getId());

        assertEquals("ali", house.getApartemantList().get(1).getOwner().getFirstName());
        assertEquals("alavi", house.getApartemantList().get(1).getOwner().getLastName());
        assertEquals("091234567", house.getApartemantList().get(1).getOwner().getMobileNumber());

        assertEquals("abas", house.getApartemantList().get(1).getRoomer().getFirstName());
        assertEquals("asadi", house.getApartemantList().get(1).getRoomer().getLastName());
        assertEquals("09123400", house.getApartemantList().get(1).getRoomer().getMobileNumber());

        assertEquals("s", house.getApartemantList().get(1).getLocation());
        assertEquals(3000, house.getApartemantList().get(1).getRimanIn());
        assertEquals(200, house.getApartemantList().get(1).getSpace());
        assertEquals(3, house.getApartemantList().get(1).getPersonNumber());
        assertEquals(200000, house.getApartemantList().get(1).getCharge());

        assertEquals(0, house.getHouseAccount().getId());
        assertEquals(543, house.getHouseAccount().getAccountNumber());
        assertEquals("meli", house.getHouseAccount().getNameBank());
        assertEquals(876, house.getHouseAccount().getCardNumber());

    }

    @Test
    public void testSetApartemantList() {
        // fail("Not yet implemented");
        Owner owner = new Owner("ali", "alavi", "091234567");
        Roomer roomer = new Roomer("abas", "alavi", "091234567");
        Apartemant apartemant1 = new Apartemant(roomer, owner, "e", 2000, 1000, 2, 200000);

        Owner owner1 = new Owner("ali", "alavi", "091234567");
        Roomer roomer1 = new Roomer("hasan", "asadi", "09123400");
        Apartemant apartemant2 = new Apartemant(roomer1, owner1, "s", 30000, 200, 3, 200000);
        List<Apartemant> apartemantList = new ArrayList<Apartemant>();
        apartemantList.add(apartemant1);
        apartemantList.add(apartemant2);
        House house1 = new House();
        house1.setApartemantList(apartemantList);

        assertEquals(0, house1.getApartemantList().get(0).getId());

        assertEquals("ali", house1.getApartemantList().get(0).getOwner().getFirstName());
        assertEquals("alavi", house1.getApartemantList().get(0).getOwner().getLastName());
        assertEquals("091234567", house1.getApartemantList().get(0).getOwner().getMobileNumber());

        assertEquals("abas", house1.getApartemantList().get(0).getRoomer().getFirstName());
        assertEquals("alavi", house1.getApartemantList().get(0).getRoomer().getLastName());
        assertEquals("091234567", house1.getApartemantList().get(0).getRoomer().getMobileNumber());

        assertEquals("e", house1.getApartemantList().get(0).getLocation());
        assertEquals(2000, house1.getApartemantList().get(0).getRimanIn());
        assertEquals(1000, house1.getApartemantList().get(0).getSpace());
        assertEquals(2, house1.getApartemantList().get(0).getPersonNumber());
        assertEquals(200000, house1.getApartemantList().get(0).getCharge());

        assertEquals(0, house1.getApartemantList().get(1).getId());

        assertEquals("ali", house1.getApartemantList().get(1).getOwner().getFirstName());
        assertEquals("alavi", house1.getApartemantList().get(1).getOwner().getLastName());
        assertEquals("091234567", house1.getApartemantList().get(1).getOwner().getMobileNumber());

        assertEquals("hasan", house1.getApartemantList().get(1).getRoomer().getFirstName());
        assertEquals("asadi", house1.getApartemantList().get(1).getRoomer().getLastName());
        assertEquals("09123400", house1.getApartemantList().get(1).getRoomer().getMobileNumber());

        assertEquals("s", house1.getApartemantList().get(1).getLocation());
        assertEquals(30000, house1.getApartemantList().get(1).getRimanIn());
        assertEquals(200, house1.getApartemantList().get(1).getSpace());
        assertEquals(3, house1.getApartemantList().get(1).getPersonNumber());
        assertEquals(200000, house1.getApartemantList().get(1).getCharge());


    }

}