package com.example.postgresdemo.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class ApartemantTest {

    @Test
    public void testApartemant() {
        Apartemant apartemant = new Apartemant();
        assertEquals(0, apartemant.getId());
        assertEquals(null, apartemant.getOwner());
        assertEquals(null, apartemant.getRoomer());
        assertEquals(null, apartemant.getLocation());
        assertEquals(0, apartemant.getRimanIn());
        assertEquals(0, apartemant.getSpace());
        assertEquals(0, apartemant.getPersonNumber());
        assertEquals(0, apartemant.getCharge());
    }

    @Test
    public void testApartemantRoomerOwnerStringLongIntIntLong() {
        //fail("Not yet implemented");
        Owner owner = new Owner("ali", "alavi", "091234567");
        Roomer roomer = new Roomer("abas", "alavi", "091289000");
        Apartemant apartemant = new Apartemant(roomer, owner, "w", 2000, 100, 2, 200000);
        assertEquals(0, apartemant.getId());

        assertEquals("ali", apartemant.getOwner().getFirstName());
        assertEquals("alavi", apartemant.getOwner().getLastName());
        assertEquals("091234567", apartemant.getOwner().getMobileNumber());

        assertEquals("abas", apartemant.getRoomer().getFirstName());
        assertEquals("alavi", apartemant.getRoomer().getLastName());
        assertEquals("091289000", apartemant.getRoomer().getMobileNumber());

        assertEquals("w", apartemant.getLocation());
        assertEquals(2000, apartemant.getRimanIn());
        assertEquals(100, apartemant.getSpace());
        assertEquals(2, apartemant.getPersonNumber());
        assertEquals(200000, apartemant.getCharge());

    }

    @Test
    public void testGetId() {
        Apartemant apartemant = new Apartemant();
        apartemant.setId(1);
        assertEquals(1, apartemant.getId());
    }

    @Test
    public void testSetId() {
        //fail("Not yet implemented");
        Owner owner = new Owner("ali", "alavi", "091234567");
        Roomer roomer = new Roomer("abas", "alavi", "091289000");
        Apartemant apartemant = new Apartemant(roomer, owner, "w", 2000, 100, 2, 200000);
        apartemant.setId(2);

        assertEquals(2, apartemant.getId());
    }

    @Test
    public void testGetOwner() {
        //fail("Not yet implemented");
        Owner owner = new Owner("ali", "alavi", "091234567");
        Roomer roomer = new Roomer("abas", "alavi", "091289000");
        Apartemant apartemant = new Apartemant(roomer, owner, "w", 2000, 100, 2, 200000);
        assertEquals("ali", apartemant.getOwner().getFirstName());
        assertEquals("alavi", apartemant.getOwner().getLastName());
        assertEquals("091234567", apartemant.getOwner().getMobileNumber());
    }

    @Test
    public void testSetOwner() {
        //fail("Not yet implemented");
        Apartemant apartemant = new Apartemant();
        Owner owner = new Owner("ali", "alavi", "091234567");
        apartemant.setOwner(owner);
        assertEquals("ali", apartemant.getOwner().getFirstName());
        assertEquals("alavi", apartemant.getOwner().getLastName());
        assertEquals("091234567", apartemant.getOwner().getMobileNumber());
    }

    @Test
    public void testGetRoomer() {
        //fail("Not yet implemented");
        Owner owner = new Owner("ali", "alavi", "091234567");
        Roomer roomer = new Roomer("abas", "alavi", "091289000");
        Apartemant apartemant = new Apartemant(roomer, owner, "w", 2000, 100, 2, 200000);

        assertEquals("abas", apartemant.getRoomer().getFirstName());
        assertEquals("alavi", apartemant.getRoomer().getLastName());
        assertEquals("091289000", apartemant.getRoomer().getMobileNumber());
    }

    @Test
    public void testSetRoomer() {
        //fail("Not yet implemented");
        Apartemant apartemant = new Apartemant();
        Roomer roomer = new Roomer("abas", "alavi", "091289000");
        apartemant.setRoomer(roomer);
        assertEquals("abas", apartemant.getRoomer().getFirstName());
        assertEquals("alavi", apartemant.getRoomer().getLastName());
        assertEquals("091289000", apartemant.getRoomer().getMobileNumber());

    }

    @Test
    public void testGetLocation() {
        //fail("Not yet implemented");
        Owner owner = new Owner("ali", "alavi", "091234567");
        Roomer roomer = new Roomer("abas", "alavi", "091289000");
        Apartemant apartemant = new Apartemant(roomer, owner, "w", 2000, 100, 2, 200000);

        assertEquals("w", apartemant.getLocation());
    }

    @Test
    public void testSetLocation() {
        //fail("Not yet implemented");
        Apartemant apartemant = new Apartemant();
        apartemant.setLocation("e");
        assertEquals("e", apartemant.getLocation());
    }

    @Test
    public void testGetRimanIn() {
        //fail("Not yet implemented");
        Owner owner = new Owner("ali", "alavi", "091234567");
        Roomer roomer = new Roomer("abas", "alavi", "091289000");
        Apartemant apartemant = new Apartemant(roomer, owner, "w", 2000, 100, 2, 200000);

        assertEquals(2000, apartemant.getRimanIn());
    }

    @Test
    public void testSetRimanIn() {
        //fail("Not yet implemented");
        Apartemant apartemant = new Apartemant();
        apartemant.setRimanIn(300);
        assertEquals(300, apartemant.getRimanIn());
    }

    @Test
    public void testGetSpace() {
        //fail("Not yet implemented");
        Owner owner = new Owner("ali", "alavi", "091234567");
        Roomer roomer = new Roomer("abas", "alavi", "091289000");
        Apartemant apartemant = new Apartemant(roomer, owner, "w", 2000, 100, 2, 200000);

        assertEquals(100, apartemant.getSpace());
    }

    @Test
    public void testSetSpace() {
        //fail("Not yet implemented");
        Apartemant apartemant = new Apartemant();
        apartemant.setSpace(200);
        assertEquals(200, apartemant.getSpace());
    }

    @Test
    public void testGetPersonNumber() {
        //fail("Not yet implemented");
        Owner owner = new Owner("ali", "alavi", "091234567");
        Roomer roomer = new Roomer("abas", "alavi", "091289000");
        Apartemant apartemant = new Apartemant(roomer, owner, "w", 2000, 100, 2, 200000);

        assertEquals(2, apartemant.getPersonNumber());
    }

    @Test
    public void testSetPersonNumber() {
        //fail("Not yet implemented");
        Apartemant apartemant = new Apartemant();
        apartemant.setPersonNumber(4);
        assertEquals(4, apartemant.getPersonNumber());
    }

    @Test
    public void testGetCharge() {
        //fail("Not yet implemented");
        Owner owner = new Owner("ali", "alavi", "091234567");
        Roomer roomer = new Roomer("abas", "alavi", "091289000");
        Apartemant apartemant = new Apartemant(roomer, owner, "w", 2000, 100, 2, 200000);

        assertEquals(200000, apartemant.getCharge());
    }

    @Test
    public void testSetCharge() {
        //fail("Not yet implemented");
        Apartemant apartemant = new Apartemant();
        apartemant.setCharge(10000);
        assertEquals(10000, apartemant.getCharge());
    }
}