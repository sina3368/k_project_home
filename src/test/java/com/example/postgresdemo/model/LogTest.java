package com.example.postgresdemo.model;

import org.junit.Before;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static org.junit.Assert.*;

public class LogTest {
    String time;
    Log log;

    @Before
    public void setUp() throws Exception {
         time = new SimpleDateFormat("yyyy.MM.dd  HH:mm:ss").format(Calendar.getInstance().getTime());
         log = new Log(" find all houses", time);
    }

    @Test
    public void testLog() {
        Log log = new Log();
        assertEquals(0, log.getId());
        assertEquals(null, log.getStatus());
        assertEquals(null, log.getTime());
    }

    @Test
    public void testLogLongStringString() {
        assertEquals(0, log.getId());
        assertEquals(" find all houses", log.getStatus());
        assertEquals(time, log.getTime());
    }


    @Test
    public void getId() {
        assertEquals(0, log.getId());
        log.setId(1);
        assertEquals(1, log.getId());

    }

    @Test
    public void setId() {
        assertEquals(0, log.getId());
        log.setId(2);
        assertEquals(2, log.getId());
    }

    @Test
    public void getStatus() {
        assertEquals(" find all houses", log.getStatus());
    }

    @Test
    public void setStatus() {
        assertEquals(" find all houses", log.getStatus());
        log.setStatus(" find house by" + 3 + "id");
        assertEquals(" find house by" + 3 + "id", log.getStatus());
    }

    @Test
    public void getTime() {
        assertEquals(time, log.getTime());
    }

    @Test
    public void setTime() {
        assertEquals(time, log.getTime());
       String time1 = new SimpleDateFormat("yyyy.MM.dd  HH:mm:ss").format(Calendar.getInstance().getTime());
       log.setTime(time1);
        assertEquals(time1, log.getTime());
    }

    }