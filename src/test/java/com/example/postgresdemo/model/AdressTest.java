package com.example.postgresdemo.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class AdressTest {

    @Test
    public void testAdress() {
        Adress adress = new Adress();
        assertEquals(0, adress.getId());
        assertEquals(null, adress.getState());
        assertEquals(null, adress.getCity());
        assertEquals(null, adress.getArea());
        assertEquals(null, adress.getPostAdress());
        assertEquals(null, adress.getPostCode());

    }

    @Test
    public void testAdressStringStringStringStringString() {
        //fail("Not yet implemented");
        Adress adress = new Adress("thran", "thran", "serag", "]eraghi", "123456");
        assertEquals(0, adress.getId());
        assertEquals("thran", adress.getState());
        assertEquals("thran", adress.getCity());
        assertEquals("serag", adress.getArea());
        assertEquals("]eraghi", adress.getPostAdress());
        assertEquals("123456", adress.getPostCode());
    }

    @Test
    public void testGetId() {
        Adress adress = new Adress();
        adress.setId(1);
        assertEquals(1, adress.getId());

    }

    @Test
    public void testSetId() {
        //fail("Not yet implemented");
        Adress adress = new Adress();
        adress.setId(2);
        assertEquals(2, adress.getId());
    }

    @Test
    public void testGetState() {
        //fail("Not yet implemented");
        Adress adress = new Adress("thran", "thran", "serag", "]eraghi", "123456");
        assertEquals("thran", adress.getState());

    }

    @Test
    public void testSetState() {
        //fail("Not yet implemented");
        Adress adress = new Adress();
        adress.setState("fars");
        assertEquals("fars", adress.getState());
    }

    @Test
    public void testGetCity() {
        //fail("Not yet implemented");
        Adress adress = new Adress("thran", "thran", "serag", "]eraghi", "123456");
        assertEquals("thran", adress.getCity());
    }

    @Test
    public void testSetCity() {
        //fail("Not yet implemented");
        Adress adress = new Adress();
        adress.setCity("shiraz");
        assertEquals("shiraz", adress.getCity());
    }

    @Test
    public void testGetArea() {
        //fail("Not yet implemented");
        Adress adress = new Adress("thran", "thran", "serag", "]eraghi", "123456");
        assertEquals("serag", adress.getArea());
    }

    @Test
    public void testSetArea() {
        //fail("Not yet implemented");
        Adress adress = new Adress();
        adress.setArea("vanak");
        assertEquals("vanak", adress.getArea());
    }

    @Test
    public void testGetPostAdress() {
        //fail("Not yet implemented");
        Adress adress = new Adress("thran", "thran", "serag", "]eraghi", "123456");
        assertEquals("]eraghi", adress.getPostAdress());

    }

    @Test
    public void testSetPostAdress() {
        //fail("Not yet implemented");
        Adress adress = new Adress();
        adress.setPostAdress("kooche 12");
        assertEquals("kooche 12", adress.getPostAdress());
    }

    @Test
    public void testGetPostCode() {
        //fail("Not yet implemented");
        Adress adress = new Adress("thran", "thran", "serag", "]eraghi", "123456");
        assertEquals("123456", adress.getPostCode());
    }

    @Test
    public void testSetPostCode() {
        //fail("Not yet implemented");
        Adress adress = new Adress();
        adress.setPostCode("12345");
        assertEquals("12345", adress.getPostCode());
    }

}