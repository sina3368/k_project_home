package com.example.postgresdemo.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class RoomerTest {

    @Test
    public void testRoomer() {
        Roomer rommer = new Roomer();
        assertEquals(null, rommer.getFirstName());
        assertEquals(null, rommer.getLastName());
        assertEquals(null, rommer.getMobileNumber());
    }

    @Test
    public void testRoomerLongStringStringString() {
        //fail("Not yet implemented");
        Roomer rommer = new Roomer("ali", "alavi", "091234567");
        assertEquals("ali", rommer.getFirstName());
        assertEquals("alavi", rommer.getLastName());
        assertEquals("091234567", rommer.getMobileNumber());
    }

    @Test
    public void testGetFirstName() {
        Roomer rommer = new Roomer("ali", "alavi", "091234567");
        assertEquals("ali", rommer.getFirstName());
    }

    @Test
    public void testSetFirstName() {
        //fail("Not yet implemented");
        Roomer rommer = new Roomer();
        rommer.setFirstName("hasan");
        assertEquals("hasan", rommer.getFirstName());
    }

    @Test
    public void testGetLastName() {
        //fail("Not yet implemented");
        Roomer rommer = new Roomer("ali", "alavi", "091234567");
        assertEquals("alavi", rommer.getLastName());
    }

    @Test
    public void testSetLastName() {
        //fail("Not yet implemented");
        Roomer rommer = new Roomer();
        rommer.setLastName("mosavi");
        assertEquals("mosavi", rommer.getLastName());
    }

    @Test
    public void testGetMobileNumber() {
        //fail("Not yet implemented");
        Roomer rommer = new Roomer("ali", "alavi", "091234567");

        assertEquals("091234567", rommer.getMobileNumber());
    }

    @Test
    public void testSetMobileNumber() {
        //fail("Not yet implemented");
        Roomer rommer = new Roomer();
        rommer.setMobileNumber("091287655");
        assertEquals("091287655", rommer.getMobileNumber());
    }

}